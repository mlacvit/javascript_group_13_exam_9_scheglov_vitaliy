import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NewCoctailComponent } from './new-coctail/new-coctail.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'new', component: NewCoctailComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
