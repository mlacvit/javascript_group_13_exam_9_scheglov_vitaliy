import { Component, OnDestroy, OnInit } from '@angular/core';
import { CoctailModel } from '../coctail.model';
import { Subscription } from 'rxjs';
import { CoctailServices } from '../coctail.services';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  coctails: CoctailModel[] | null = null;
  coctailsOne!: CoctailModel | null;
  cocId = '';
  change!: Subscription;
  isFetch = false;
  fetchSubscription!: Subscription;

  constructor(public service: CoctailServices, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.service.getCoctails();
    this.change = this.service.changeCoct.subscribe((coc: CoctailModel[]) => {
      this.coctails = coc;
    });

    this.fetchSubscription = this.service.cocFetching.subscribe((isfetching: boolean) => {
      this.isFetch = isfetching;
    });
  }


  goId(id: string) {
    this.cocId = id;
    return  this.service.getCoctailOne(this.cocId);
  }

  getIngri(): any{
    return this.service.coctailOne?.ingredients
  }

  ngOnDestroy(): void {
    this.change.unsubscribe();
    this.fetchSubscription.unsubscribe();
  }


}
