import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';

export const ValidateUrlDirective = (): ValidatorFn => {
  return (control: AbstractControl): ValidationErrors | null => {
    const urlFormat = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/.test(control.value);
    if (urlFormat){
      return null
    }
    return {urlError: true};
  }
}

@Directive({
  selector: '[appUrl]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: urlValidatorDirective,
    multi: true
  }]
})

export class urlValidatorDirective implements Validator{
  validate(control: AbstractControl): ValidationErrors | null {
    return ValidateUrlDirective()(control);
  }
}
