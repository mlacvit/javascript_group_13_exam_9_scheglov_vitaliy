import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NewCoctailComponent } from './new-coctail/new-coctail.component';
import { CoctailServices } from './coctail.services';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { urlValidatorDirective } from './validate-url.directive';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NewCoctailComponent,
    urlValidatorDirective,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,

  ],
  providers: [CoctailServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
