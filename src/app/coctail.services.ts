import { Injectable } from '@angular/core';
import { map, Subject, tap } from 'rxjs';
import { CoctailModel } from './coctail.model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable()
export class CoctailServices {
  coctails: CoctailModel[] | null = null;
  coctailOne!: CoctailModel | null;
  changeCoct = new Subject<CoctailModel[]>()
  loader = new Subject<boolean>();
  cocFetching = new Subject<boolean>();

  constructor(private http: HttpClient, private router: Router) {}

  getCoctails() {
    this.cocFetching.next(true);
    this.http.get<{[id: string]: CoctailModel}>('https://mlacvit-10af9-default-rtdb.firebaseio.com/coctail.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const data = result[id];
          return new CoctailModel(
            id,
            data.title,
            data.url,
            data.type,
            data.description,
            data.ingredients,
            data.descriptionCoctail,
          )
        })
      }))
      .subscribe((coc: CoctailModel[]) => {
        this.coctails = coc;
        this.changeCoct.next(this.coctails.slice());
        this.cocFetching.next(false);
      }, error => {
        this.cocFetching.next(false);
      });
  };

  getCoctailOne(id: string) {
    return this.http.get<CoctailModel>(`https://mlacvit-10af9-default-rtdb.firebaseio.com/coctail/${id}.json`)
      .pipe(map(result => {
        if (result === null){
          return null;
        }
        return new CoctailModel(
          id,
          result.title,
          result.url,
          result.type,
          result.description,
          result.ingredients,
          result.descriptionCoctail
        );
      })).subscribe((coc) =>{
       this.coctailOne = coc;
      });
  };

  sendInfoCoctail(body: CoctailModel){
    this.loader.next(true);
    this.http.post(`https://mlacvit-10af9-default-rtdb.firebaseio.com/coctail.json`, body).pipe(
      tap(() => {
        this.loader.next(false);
      }, error => {
        this.loader.next(false);
      })
    ).subscribe( () => {
      this.getCoctails();
      this.router.navigate(['/']);
    });
  };
}
