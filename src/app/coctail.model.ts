
import { IngredientModel } from './ingredient.model';

export class CoctailModel {
  constructor(
    public id: string,
    public title: string,
    public url: string,
    public type: string,
    public description: string,
    public ingredients: IngredientModel,
    public descriptionCoctail: string,
  ) {}

}
