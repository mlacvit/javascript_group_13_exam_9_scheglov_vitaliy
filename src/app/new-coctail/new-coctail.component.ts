import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { CoctailServices } from '../coctail.services';
import { CoctailModel } from '../coctail.model';
import { ValidateUrlDirective } from '../validate-url.directive';

@Component({
  selector: 'app-new-coctail',
  templateUrl: './new-coctail.component.html',
  styleUrls: ['./new-coctail.component.css']
})
export class NewCoctailComponent implements OnInit, OnDestroy {
  formGroupCoctail!: FormGroup;
  fetchingSubscriptions!: Subscription;
  isFetching: boolean = false;

  constructor(private service: CoctailServices) { }

  ngOnInit(): void {
    this.formGroupCoctail = new FormGroup({
      title: new FormControl('', Validators.required),
      url: new FormControl('',
        [Validators.required,
          ValidateUrlDirective()
        ],

      ),
      type: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      ingredients: new FormArray([]),
      descriptionCoctail: new FormControl('', Validators.required),
    });

    this.fetchingSubscriptions = this.service.loader.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
  }

  errorCoctail(fieldName: string, errorField: string) {
    const field = this.formGroupCoctail.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorField]);
  };

  sendCoctail() {
    const id = Math.random().toString();
    const body = new CoctailModel(
      id,
      this.formGroupCoctail.value.title,
      this.formGroupCoctail.value.url,
      this.formGroupCoctail.value.type,
      this.formGroupCoctail.value.description,
      this.formGroupCoctail.value.ingredients,
      this.formGroupCoctail.value.descriptionCoctail,
    );
    this.service.sendInfoCoctail(body);
  };

  addIngri() {
    const ingri = <FormArray>this.formGroupCoctail.get('ingredients');
    const stepGroup = new FormGroup({
      name: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required),
      measure: new FormControl('', Validators.required),
    });
    ingri.push(stepGroup);
  }

  getIngri() {
    const ingre = <FormArray>this.formGroupCoctail.get('ingredients');
    return ingre.controls;
  }

  ngOnDestroy(): void {
    this.fetchingSubscriptions.unsubscribe();
  }

  remIngri(i: number) {
    const ingre = <FormArray>this.formGroupCoctail.get('ingredients');
    ingre.removeAt(i)
  }
}
